# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from .models import Tweet

from django.views.generic import (
	TemplateView, 
	ListView
	)


class LandingPageView(TemplateView):
	template_name = "base/index.html"

class AboutPageView(TemplateView):
	template_name = "base/about.html"

class TweetListView(ListView):
	model = Tweet

	def get_queryset(self):
		return Tweet.objects.all()