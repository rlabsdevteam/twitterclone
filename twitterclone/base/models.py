# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Tweet(models.Model):
	title = models.CharField(max_length=280)
	image = models.ImageField(upload_to="media/tweet_pics")

class Checkin(models.Model):
	name = models.CharField(max_length=280)
	surname = models.CharField(max_length=280)
	profile_pic = models.ImageField(upload_to="media/profile_pic")
	checkin_number = models.IntegerField(default=0)
	# this will automatically add a date and time when someone checks in
	timestamp = models.DateTimeField(auto_now_add=True) 