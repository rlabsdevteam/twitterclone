from django.conf.urls import url, include

from .views import *

app_name = "base"

urlpatterns = [
    url(r'^$', LandingPageView.as_view(), name="index"),
    url(r'^about/$', AboutPageView.as_view(), name="about"),
    url(r'^list/tweets/$', TweetListView.as_view(), name="list-tweets"),
]
