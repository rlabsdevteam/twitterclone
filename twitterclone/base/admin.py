# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import *

@admin.register(Tweet)
class TweetAdmin(admin.ModelAdmin):
	list_display = ('title',)

@admin.register(Checkin)
class CheckinAdmin(admin.ModelAdmin):
	list_display = ('name', 'surname', 'checkin_number')
	search_fields = ('name', 'surname')
	list_filter = ('timestamp', 'checkin_number')
