# README #

This is the base project for the first webpublishing class to include Django as a framework to publish a website.


### How do I get set up? ###

> cd ./twitterclone

> source ./env/bin/activate

> pip install -r ./requirements.txt

> cd ./twitterclone

> ./manage.py createsuperuser

> ./manage.py makemigrations && ./manage.py migrate

> ./manage.py migrate

> ./manage.py runserver

### Project Owners

1. Kurt Appolis - kurt@rlabs.org

2. Neil Fouten - neil@rlabs.org

